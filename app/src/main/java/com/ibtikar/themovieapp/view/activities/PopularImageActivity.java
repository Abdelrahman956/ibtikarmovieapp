package com.ibtikar.themovieapp.view.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.ibtikar.themovieapp.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.ibtikar.themovieapp.helper.Constants.PERSON_IMAGE;

public class PopularImageActivity extends AppCompatActivity {
    String person_image;
    ImageView popular_image, back;
    ImageButton download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_image);
        popular_image = findViewById(R.id.popular_image);
        back = findViewById(R.id.back);
        download = findViewById(R.id.download);
        Intent intent = getIntent();
        person_image = intent.getStringExtra(PERSON_IMAGE);
        Glide.with(this).load(person_image).into(popular_image);

        popular_image.setOnLongClickListener(v -> {
            downloadImage();
            return false;
        });

        download.setOnClickListener(view -> {
            downloadImage();
        });

        back.setOnClickListener(view -> {
            finish();
        });
    }

    private void downloadImage() {
        Toast.makeText(PopularImageActivity.this, "Photo saved to the device", Toast.LENGTH_SHORT).show();
        Glide.with(this)
                .asBitmap()
                .load(person_image)
                .into(new SimpleTarget<Bitmap>() {
                    @RequiresApi(api = Build.VERSION_CODES.Q)
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> transition) {
                        saveToInternalStorage(resource);
//                        ContentValues values = new ContentValues();
//
//                        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
//                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
//                        values.put(MediaStore.MediaColumns.DATA, saveToInternalStorage(resource));
//                        getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private String saveToInternalStorage(Bitmap bitmapImage) {
        File myPath = new File(getExternalFilesDir(null), System.currentTimeMillis() + "Popular.jpg");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.e("MYPath", "MYPath " + myPath.getAbsolutePath());
        Toast.makeText(PopularImageActivity.this, myPath.getAbsolutePath(), Toast.LENGTH_SHORT).show();

        return myPath.getAbsolutePath();
    }
}