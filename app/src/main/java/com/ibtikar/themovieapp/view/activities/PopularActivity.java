package com.ibtikar.themovieapp.view.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.ibtikar.themovieapp.R;
import com.ibtikar.themovieapp.adapters.PopularAdapter;
import com.ibtikar.themovieapp.viewmodel.PopularViewModel;
import com.wang.avi.AVLoadingIndicatorView;

public class PopularActivity extends AppCompatActivity {
    RecyclerView popularRecycler;
    PopularViewModel popularViewModel;
    private int pageNumber = 1;
    boolean isLoading = true;
    int pastVisibleItem = 0, visibleItemCount = 0, totalItemCount = 0, previousTotal = 0;
    int viewThreshold = 10;
    private GridLayoutManager layoutManager;
    PopularAdapter popularAdapter;
    private AVLoadingIndicatorView avi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular);

        avi = findViewById(R.id.avi);
        popularRecycler = findViewById(R.id.popular_recycler);

        layoutManager = new GridLayoutManager(this, 1);

        popularViewModel = ViewModelProviders.of(this).get(PopularViewModel.class);

        avi.show();
        popularViewModel.popularLiveData(this, pageNumber).observe(this, popularModel -> {

            popularRecycler.setLayoutManager(layoutManager);
            popularAdapter = new PopularAdapter(this, popularModel);
            popularRecycler.setAdapter(popularAdapter);
            avi.hide();
            popularRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisibleItem = layoutManager.findFirstVisibleItemPosition();
                    Log.e("TAG", "dy " + dy);
                    if (dy > 0) {
                        if (isLoading) {
                            if (totalItemCount > previousTotal) {
                                isLoading = false;
                                previousTotal = totalItemCount;
                            }
                        }
                        if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleItem + viewThreshold)) {
                            Log.e("TAG", "performPagination()");
                            avi.smoothToShow();

                            pageNumber++;
                            popularViewModel.popularLiveData(PopularActivity.this, pageNumber).observe(PopularActivity.this, popularModel -> {

                                popularAdapter.addInRecycle(popularModel.getResults());
                                avi.smoothToHide();

                            });
                            isLoading = true;

                            Log.e("TAG", "pageNumber " + pageNumber);

                        }
                    }
                }
            });

        });
    }
}