package com.ibtikar.themovieapp.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibtikar.themovieapp.R;
import com.ibtikar.themovieapp.viewmodel.PopularDetailsViewModel;
import com.wang.avi.AVLoadingIndicatorView;

import static com.ibtikar.themovieapp.helper.Constants.IMAGE_URL;
import static com.ibtikar.themovieapp.helper.Constants.PERSON_ID;
import static com.ibtikar.themovieapp.helper.Constants.PERSON_IMAGE;

public class PopularDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView popular_image, back;
    TextView name, department, birthday, place_of_birth, biography;
    PopularDetailsViewModel popularDetailsViewModel;
    private AVLoadingIndicatorView avi;
    int person_id;
    String person_image;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_details);

        Intent intent = getIntent();
        person_id = intent.getIntExtra(PERSON_ID, 0);

        avi = findViewById(R.id.avi);
        back = findViewById(R.id.back);
        popular_image = findViewById(R.id.popular_image);
        name = findViewById(R.id.name);
        department = findViewById(R.id.department);
        birthday = findViewById(R.id.birthday);
        place_of_birth = findViewById(R.id.place_of_birth);
        biography = findViewById(R.id.biography);

        popularDetailsViewModel = ViewModelProviders.of(this).get(PopularDetailsViewModel.class);

        avi.show();
        popularDetailsViewModel.popularDetailsLiveData(this, person_id).observe(this, popularModel -> {
            name.setText(getString(R.string.name) + " " + popularModel.getName());
            department.setText(getString(R.string.department) + " " + popularModel.getKnown_for_department());
            birthday.setText(getString(R.string.birthday) + " " + popularModel.getBirthday());
            place_of_birth.setText(getString(R.string.place_of_birth) + " " + popularModel.getPlace_of_birth());
            biography.setText(getString(R.string.biography) + " " + popularModel.getBiography());
            person_image = IMAGE_URL + popularModel.getProfile_path();
            Glide.with(this).load(person_image).into(popular_image);
           avi.hide();
        });
        back.setOnClickListener(this);
        popular_image.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            finish();
        }
        if (view == popular_image) {
            Intent intent = new Intent(PopularDetailsActivity.this, PopularImageActivity.class);
            intent.putExtra(PERSON_IMAGE, person_image);
            startActivity(intent);
        }
    }
}