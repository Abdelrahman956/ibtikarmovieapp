package com.ibtikar.themovieapp.helper;

public class Constants {
    public static final String API_LINK = "https://api.themoviedb.org/3/";
    public static final String PERSON_ID = "PERSON_ID";
    public static final String PERSON_IMAGE = "PERSON_IMAGE";
    public static final String API_KEY = "2085e7f3739ce2a3492baab4ea84629a";
    public static final String LANGUAGE = "en-US";
    public static final String IMAGE_URL = "https://image.tmdb.org/t/p/original";
}
