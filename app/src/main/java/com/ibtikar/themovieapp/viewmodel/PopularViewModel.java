package com.ibtikar.themovieapp.viewmodel;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ibtikar.themovieapp.R;
import com.ibtikar.themovieapp.apis.RetrofitClient;
import com.ibtikar.themovieapp.model.PopularModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.ibtikar.themovieapp.helper.Constants.API_KEY;
import static com.ibtikar.themovieapp.helper.Constants.LANGUAGE;

public class PopularViewModel extends ViewModel {

    private MutableLiveData<PopularModel> popularLiveDataMutableLiveData;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LiveData<PopularModel> popularLiveData(Context context, int pageNumber) {
        popularLiveDataMutableLiveData = new MutableLiveData<>();
        loadPopular(context, pageNumber);


        return popularLiveDataMutableLiveData;
    }

    private void loadPopular(Context context, int pageNumber) {

        Observable<PopularModel> observable = RetrofitClient.getInstance(context)
                .getApi().getPopular(LANGUAGE, API_KEY, pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        compositeDisposable.add(observable.subscribe(o -> {
            if (o.getResults() != null) {

                popularLiveDataMutableLiveData.setValue(o);
            } else {

                Toast.makeText(context, R.string.empty_list, Toast.LENGTH_SHORT).show();
            }

        }, e -> {

            Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show();
        }));

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}