package com.ibtikar.themovieapp.viewmodel;

import android.content.Context;
import android.widget.Toast;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ibtikar.themovieapp.R;
import com.ibtikar.themovieapp.apis.RetrofitClient;
import com.ibtikar.themovieapp.model.PopularDetailsModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.ibtikar.themovieapp.helper.Constants.API_KEY;
import static com.ibtikar.themovieapp.helper.Constants.LANGUAGE;

public class PopularDetailsViewModel extends ViewModel {

    private MutableLiveData<PopularDetailsModel> popularLiveDataMutableLiveData;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public LiveData<PopularDetailsModel> popularDetailsLiveData(Context context, int person_id) {
        popularLiveDataMutableLiveData = new MutableLiveData<>();
        loadPopularDetails(context, person_id);
        return popularLiveDataMutableLiveData;
    }

    private void loadPopularDetails(Context context, int person_id) {

        Observable<PopularDetailsModel> observable = RetrofitClient.getInstance(context)
                .getApi().getPopularDetails(person_id, API_KEY, LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        compositeDisposable.add(observable.subscribe(o -> {
            if (o != null) {

                popularLiveDataMutableLiveData.setValue(o);
            } else {

                Toast.makeText(context, R.string.no_data, Toast.LENGTH_SHORT).show();
            }

        }, e -> {

            Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show();
        }));

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

}
