package com.ibtikar.themovieapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ibtikar.themovieapp.R;
import com.ibtikar.themovieapp.model.PopularModel;
import com.ibtikar.themovieapp.view.activities.PopularDetailsActivity;

import java.util.List;

import static com.ibtikar.themovieapp.helper.Constants.PERSON_ID;

public class PopularAdapter extends RecyclerView.Adapter<PopularAdapter.PopularViewHolder> {
    private Context context;
    private PopularModel popularModel;

    public PopularAdapter(Context context, PopularModel popularModel) {
        this.context = context;
        this.popularModel = popularModel;
    }

    @NonNull
    @Override
    public PopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.popular_row, parent, false);
        return new PopularViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PopularViewHolder holder, int position) {
        holder.name.setText(context.getString(R.string.name) + " " + popularModel.getResults().get(position).getName());
        holder.department.setText(context.getString(R.string.department) + " " + popularModel.getResults().get(position).getKnown_for_department());
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, PopularDetailsActivity.class);
            intent.putExtra(PERSON_ID, popularModel.getResults().get(position).getId());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return popularModel.getResults().size();
    }

    public void addInRecycle(List<PopularModel.Results> results) {
        this.popularModel.getResults().addAll(results);
        notifyDataSetChanged();
    }

    public class PopularViewHolder extends RecyclerView.ViewHolder {
        TextView name, department;

        private PopularViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            department = itemView.findViewById(R.id.department);

        }
    }
}
