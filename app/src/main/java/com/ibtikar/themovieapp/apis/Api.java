package com.ibtikar.themovieapp.apis;

import com.ibtikar.themovieapp.model.PopularDetailsModel;
import com.ibtikar.themovieapp.model.PopularModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface Api {

    @GET("person/popular")
    Observable<PopularModel> getPopular(@Query("language") String language,
                                        @Query("api_key") String apiKey,
                                        @Query("page") int page);

    @GET("person/{person_id}")
    Observable<PopularDetailsModel> getPopularDetails(@Path("person_id") int id,
                                                      @Query("api_key") String apiKey,
                                                      @Query("language") String language);
}