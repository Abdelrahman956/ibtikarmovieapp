package com.ibtikar.themovieapp.apis;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.ibtikar.themovieapp.helper.Constants.API_LINK;


public class RetrofitClient {
    private static Context sContext;

    private static volatile RetrofitClient mInstance;
    private Retrofit retrofit;
    private static final String TAG = "ServiceGenerator";

    public RetrofitClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        retrofit = new Retrofit.Builder()
                .client(providerOkHttpClient())
                .baseUrl(API_LINK)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .build();
    }

    //singleton
    public static RetrofitClient getInstance(Context context) {
        if (mInstance == null) {
            synchronized (RetrofitClient.class) {
                mInstance = new RetrofitClient();
                sContext = context;
            }
        }
        return mInstance;
    }


    public static OkHttpClient providerOkHttpClient() {
        OkHttpClient.Builder okhttpclient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);


        return okhttpclient.build();
    }


    public Api getApi() {
        return retrofit.create(Api.class);
    }

}